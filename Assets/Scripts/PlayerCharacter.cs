﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
	public static PlayerCharacter Instance;

	[HideInInspector]
	public Animator thisAnimComp;
	[HideInInspector]
	public Health thisHealth;
	[SerializeField]
	private float slowdownTime = .5f;
	[SerializeField]
	private Vector3 distanceThresholds;

	private void Awake()
	{
		Instance = this;
		thisAnimComp = GetComponent<Animator>();
		thisHealth = GetComponent<Health>();
	}

	public void MoveToTarget(Vector3 pos)
	{
		//Debug.Log("move");
		Vector3 vectorRef = Vector3.zero;
		transform.position = Vector3.SmoothDamp(transform.position, pos, ref vectorRef, 0.15f);
		transform.LookAt(pos);

		var dist = Vector3.Distance(pos, transform.position);
		//Debug.Log(dist);
		if(dist < distanceThresholds.x)
		{
			thisAnimComp.SetBool("Shuffling", true);
			thisAnimComp.SetBool("Walking", false);
			thisAnimComp.SetBool("Running", false);
		}

		else if(dist < distanceThresholds.y)
		{
			thisAnimComp.SetBool("Shuffling", false);
			thisAnimComp.SetBool("Walking", true);
			thisAnimComp.SetBool("Running", false);
		}

		else
		{
			thisAnimComp.SetBool("Shuffling", false);
			thisAnimComp.SetBool("Walking", false);
			thisAnimComp.SetBool("Running", true);
		}

		StopAllCoroutines();
		StartCoroutine(SlowingDown());
	}

	public void ChangeAnim(string trig)
	{
		thisAnimComp.SetTrigger(trig);
	}

	private IEnumerator SlowingDown()
	{
		yield return new WaitForSeconds(slowdownTime);
		ChangeAnim("Stop");
	}
}