﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeasantTrigger : MonoBehaviour
{
    public bool ClearWhenDead;
    public ParticleSystem Particles;
    public Vector3 DeathPosition;
    private Vector3 startPosition;
    public float Speed;
    private bool doDie;
    private Vector3 moveToPos;
    private Animator animator;
    void Start()
    {
		if (Speed == 0)
        {
            Speed = 1;
        }

        startPosition = transform.position;
        animator = GetComponent<Animator>();

		AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(0);
		animator.Play(state.fullPathHash, -1, Random.Range(0f, 1f));

		//animation["Idle"].time = Random.Range(0.0, animation["Idle"].length);
		//animator["RockThrowFixed"].time = Random.Range(0.0, animator["RockThrowFixed"].length);
	}

    public void DoDieAnimation()
    {
        doDie = true;
        transform.LookAt(DeathPosition);
        DeathPosition.y = startPosition.y;
        Particles.Play();
    }

    void LateUpdate()
    {
        if (doDie)
        {
            transform.position = Vector3.MoveTowards(transform.position, DeathPosition, Speed * Time.deltaTime);
            transform.LookAt(DeathPosition);
            if(Vector3.Distance(transform.position, DeathPosition) < 0.1f)
            {
                doDie = false;
                animator.SetTrigger("Die");
                if (ClearWhenDead)
                {
                    StartCoroutine(die());
                }
            }
            // Handle LookAt
        }
    }

    private IEnumerator die()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
