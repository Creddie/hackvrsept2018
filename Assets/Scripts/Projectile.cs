﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Tooltip("The maximum of the shot parabola.")]
    [SerializeField] private int arcHeight = 1;
    [SerializeField]
	private float projectileSpeed = 20;
    public GameObject target;
    public Vector3 targetPos;
    public AudioClip HitClip;
    private Vector3 startPos;
    [SerializeField]
	private  int damageCaused;

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        // Compute the next position, with arc added in
        float x0 = startPos.x;
        float x1 = targetPos.x;
        float dist = x1 - x0;
        float nextX = Mathf.MoveTowards(transform.position.x, x1, projectileSpeed * Time.deltaTime);
        float baseY = Mathf.Lerp(startPos.y, targetPos.y, (nextX - x0) / dist);
        float baseZ = Mathf.Lerp(startPos.z, targetPos.z, (nextX - x0) / dist);

        float arc = arcHeight * (nextX - x0) * (nextX - x1) / (-0.25f * dist * dist);
        Vector3 nextPos = new Vector3(nextX, baseY + arc, baseZ);

        // Rotate to face the next position, and then move there
        transform.rotation = Quaternion.LookRotation(nextPos - transform.position);
        transform.position = nextPos;
        // If the code reaches here, the projectile missed. Thus, delete it when it hits where the player was.
        if (nextPos == targetPos)
            Destroy(gameObject);
    }

    public void SetDamage(int damage)
    {
        if(damage != 0)
            damageCaused = damage;
    }

    void OnCollisionEnter(Collision collision)
    {
        var tagCollidedWith = collision.gameObject.tag;
        if (tagCollidedWith == "Player")
        {
            DamageIfDamageable(collision);
            Destroy(gameObject);
        }
    }

    private void DamageIfDamageable(Collision collision)
    {
		//var source = collision.gameObject.GetComponentInChildren<AudioSource>();
		//source.clip = HitClip;
		//source.Play();
        PlayerCharacter.Instance.thisHealth.TakeDamage(damageCaused);
    }
}
