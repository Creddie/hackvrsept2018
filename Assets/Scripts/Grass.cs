﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
	private void Awake()
	{
		SetRandomRotation();
		SetRandomSize();
	}

	private void SetRandomRotation()
	{
		var newY = Random.Range(0, 360f);
		transform.localEulerAngles = new Vector3(-90f, newY, 0);
	}

	private void SetRandomSize()
	{
		var vil = GetComponentInParent<Village>();
		var scal = Random.Range(vil.grassSizeMinMax.x, vil.grassSizeMinMax.y);
		transform.localScale = new Vector3(scal, scal, scal);
	}
}