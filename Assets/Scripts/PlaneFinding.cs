﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using MagicLeap;

public class PlaneFinding : MonoBehaviour
{
	public static PlaneFinding Instance;

	public Transform boundingBoxTransform;
	public Vector3 boundingBoxExtents;

	//draws this object on queried planes
	public GameObject planeVisualization;
	public GameObject player;
	[SerializeField]
	private GameObject[] possibleEnemies;
	[SerializeField]
	private float villageRespawnTime = 7f;

    [BitMask(typeof(MLWorldPlanesQueryFlags))] public MLWorldPlanesQueryFlags queryFlags;

	//The number of time the user has to look around in seconds
	private float requestTime = 5.0f;
	private float timeSinceLastReq = 0.0f;
	[SerializeField]
	private float maxY = 100f;

	private MLWorldPlanesQueryParams _params = new MLWorldPlanesQueryParams();

	//Collected planes list 
	private List<GameObject> _planesList = new List<GameObject>();

	//Horizontal planes above floors list 
	private List<GameObject> _aboveFloorList = new List<GameObject>();

	private int requestIterations = 0;

	public int playerInstantiationIterations;

    private Vector3 playerInstantiationPosition;

    private Vector3 enemyInstantiationPosition;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		MLWorldPlanes.Start();
		//Debug.Log("Started MLWorldPlanes");
		//requestPlanes();
	}

	private void OnDestroy()
	{
		MLWorldPlanes.Stop();
	}

	private void Update()
	{
		//Code to request planes every 5 seconds 
		timeSinceLastReq += Time.deltaTime;
		if (timeSinceLastReq > requestTime)
		{
			requestPlanes();
		}
	}

	private void requestPlanes()
	{
		//Sets parameters for planes searching 
		//Debug.Log("Requesting planes");
		_params.Flags = queryFlags;
		_params.MaxResults = 100;
		_params.BoundsCenter = boundingBoxTransform.position;
		_params.BoundsExtents = boundingBoxExtents;

		//Debug.Log("Query params are " + _params.ToString());
		//Debug.Log("Trying to get planes");
		//Callback to HandleReceivedPlanes
		MLWorldPlanes.GetPlanes(_params, HandleReceivedPlanes);
		//Debug.Log("Finished GetPlanes() method");
	}

	private void HandleReceivedPlanes(MLResult result, MLWorldPlane[] planes)
	{
		requestIterations++;
		//Removes current cache of collected planes, both floor and nonfloor
		//Debug.Log("In HandleRecievedPlanes function");
		//Debug.Log("There are currently " + _planesList.Count + " planes in the environment");
		for (int i = _planesList.Count - 1; i >= 0; i--)
		{
			Destroy(_planesList[i]);
			_planesList.Remove(_planesList[i]);
		}

		for (int i = _aboveFloorList.Count - 1; i >= 0; i--)
		{
			Destroy(_aboveFloorList[i]);
			_aboveFloorList.Remove(_aboveFloorList[i]);
		}

		//Creates new cache of planes
		//Debug.Log("There were " + planes.Length + " planes found");
		GameObject newPlane;
		List<float> yValues = new List<float>();
		for (int i = 0; i < planes.Length; i++)
		{
			newPlane = Instantiate(planeVisualization);
			newPlane.transform.position = planes[i].Center;
			newPlane.transform.rotation = planes[i].Rotation;

			if (newPlane.transform.position.y < maxY)
			{
				newPlane.transform.localScale = new Vector3(planes[i].Width, planes[i].Height, 1.0f);
				_planesList.Add(newPlane);
				yValues.Add(newPlane.transform.position.y);
			}

			else
				Destroy(newPlane);
		}

		float minY = yValues.Min(); //minimum y-value should be height of the floor planes 

		//Debug.Log("Minimum y-value is " + minY);
		//Finds all planes more than 50 centimeters above the floor and adds them into a nonfloor planes list
		for (int i = 0; i < _planesList.Count; i++)
		{
			if (_planesList[i].transform.position.y >= minY + 0.5f)// && _planesList[i].transform.position.y <= minY + maxY)
			{
				_aboveFloorList.Add(_planesList[i]);
			}
		}

		//Debug.Log("There are currently " + _aboveFloorList.Count + " non-floor planes visible");

		if (requestIterations == playerInstantiationIterations)
		{
			List<float> distancesNonFloorPlanes = new List<float>();
			//Debug.Log("Above floors list has " + _aboveFloorList.Count + " planes");
			for (int i = 0; i < _aboveFloorList.Count; i++)
			{
				distancesNonFloorPlanes.Add(Vector3.Distance(Camera.main.transform.position, _aboveFloorList[i].transform.position));
			}
			int minValueIndex = distancesNonFloorPlanes.IndexOf(distancesNonFloorPlanes.Min());
			playerInstantiationPosition = _aboveFloorList[minValueIndex].transform.position;

			Instantiate(player, playerInstantiationPosition, Quaternion.identity).GetComponent<BaseRaycast>();
			//Debug.Log("Player Instantiation Position is " + playerInstantiationPosition);
            HandleEnemyPlacement();
        }
	}

    private void HandleEnemyPlacement()
    {
        //Debug.Log("Above floors list has " + _aboveFloorList.Count + " planes");
        for (int i = 0; i < _planesList.Count; i++)
        {
            enemyInstantiationPosition = _planesList[i].transform.position;

			var rando = Random.Range(0, possibleEnemies.Length);
			var ene = possibleEnemies[rando];
            var spawned = Instantiate(ene, enemyInstantiationPosition, Quaternion.identity);
			//Debug.Log("Enemy1 Instantiation Position is " + enemyInstantiationPosition);
		}
    }

	public void StartRespawnEnemy(Vector3 posi, Village origin, GameObject decal)
	{
		StartCoroutine(RespawnEnemy(posi, origin, decal));
	}

	private IEnumerator RespawnEnemy(Vector3 posi, Village origin, GameObject decal)
	{
		yield return new WaitForSeconds(villageRespawnTime);

		if (null != origin)
			origin.gameObject.SetActive(false);

		else if (null != decal)
			decal.SetActive(false);

		enemyInstantiationPosition = posi;
		var rando = Random.Range(0, possibleEnemies.Length);
		var ene = possibleEnemies[rando];
		var spawned = Instantiate(ene, enemyInstantiationPosition, Quaternion.identity);
	}
}