﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
	private int damagePerShot = 9;
    [SerializeField]
	private GameObject projectileToUse;
    [SerializeField]
	private GameObject projectileSocket;
    [SerializeField]
	private float RotaionSpeed =20f;
	private float currentHealthPoints;
	private GameObject player = null;

    void Start()
    {
        player = PlayerCharacter.Instance.gameObject;
		var eve = gameObject.GetComponent<ThrowEventHandler>();
		eve.damagePerShot = damagePerShot;
		eve.projectileSocket = projectileSocket;
		eve.projectileToUse = projectileToUse;
		eve.player = player;
    }

    // Rotates the enemy to face the player, rotate only along the Y axis.
    void LateUpdate()
    {
        if (player != null)
        {
            var lookPos = player.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * RotaionSpeed);
        }
    }
}