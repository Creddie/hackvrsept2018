﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepLogic : MonoBehaviour
{
	[SerializeField]
	private GameObject fireFX;

	public void Burn()
	{
		fireFX.SetActive(true);
	}
}