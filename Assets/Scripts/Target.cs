﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
	[SerializeField]
	private GameObject reticle;

	private void Awake()
	{
		reticle.SetActive(false);
	}

	public void SetTargetedState(bool active)
    {
		reticle.SetActive(active);
    }
}
