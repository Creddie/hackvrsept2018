﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Adds health to the game object this script is attached to
 */
public class Health : MonoBehaviour {
    [Tooltip("Maximum Health Points this entity has.")]
    [SerializeField] int maxHealthPoints = 100;
    [Tooltip("The current health of the entity. If this value is set to 0 in the inspector, the script will automatically set the current health to the max healthh.")]
    public int currentHealthPoints = 0;

    // Deals damage to the owner of this script, ensures that health is never at 0 or above the maximum health.
    public void TakeDamage(int damage)
    {
        currentHealthPoints = Mathf.Clamp(currentHealthPoints - damage, 0, maxHealthPoints);

		if (0 == currentHealthPoints)
		{
			PlayerCharacter.Instance.ChangeAnim("Death");
			enabled = false;
		}

		else if (currentHealthPoints > maxHealthPoints * .75f)
			PlayerCharacter.Instance.ChangeAnim("HitSmall");

		else if (currentHealthPoints > maxHealthPoints * .25f)
			PlayerCharacter.Instance.ChangeAnim("HitMedium");

		else
			PlayerCharacter.Instance.ChangeAnim("HitBig");
	}

    void Awake () {
        if(currentHealthPoints == 0)
            currentHealthPoints = maxHealthPoints;
    }
}
