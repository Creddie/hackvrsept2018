﻿using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class EyeTargeting : MonoBehaviour {

    public LayerMask TargetLayer;
    public float Threshold; // < TODO
    public Camera Camera;
    public GameObject debugGazerayPrefab;
    public bool ShowDebugRays = true;

    private GameObject _targetedObject;
    private static Vector3 _heading;
    
	void Start ()
    {
        if (!MLEyes.IsStarted) MLEyes.Start();
        print("Started eye tracking component.");
	}

    private void OnDisable()
    {
        if (MLEyes.IsStarted) MLEyes.Stop();
    }

    void Update ()
    {
        if (MLEyes.IsStarted)
        {
            RaycastHit rayHit;
            //Vector3 newHeading = MLEyes.FixationPoint - Camera.transform.position;
            //Vector3 zero = Vector3.zero;

            //_heading = Vector3.SmoothDamp(_heading, newHeading, ref zero, 0.05f);
            _heading = MLEyes.FixationPoint;// - Camera.transform.position;


            if (ShowDebugRays) {
                GameObject GazerayClone = Instantiate(debugGazerayPrefab);
                GazerayClone.name = MLEyes.FixationPoint.ToString();
                LineRenderer lr = GazerayClone.GetComponent<LineRenderer>();
                GazerayClone.transform.position = Camera.transform.position;
                lr.SetPositions(new Vector3[] { Camera.main.transform.position, Camera.transform.position + _heading.normalized * 10 });
                Destroy(GazerayClone, 3);
            }
            

            if (Physics.Raycast(Camera.main.transform.position, _heading.normalized, out rayHit, 10.0f, TargetLayer))
            //if (Physics.SphereCast(Camera.transform.position, 0.1f, _heading, out rayHit, 10.0f, TargetLayer))
            {
                print("Ray hit an object!");
                
                GameObject _newTargetObject = rayHit.collider.gameObject;

                if (_newTargetObject == _targetedObject) return; // no change in targets, return.

                // new target, deselect previous target.
                _targetedObject?.GetComponent<Target>().SetTargetedState(false);

                // Set new target as selected.
                _newTargetObject.GetComponent<Target>().SetTargetedState(true);

                _targetedObject = _newTargetObject;

            }
            else
            {
                //print("Ray didn't hit shit.");
                // Didn't hit anything on the target layer, deselect previous target.
                _targetedObject?.GetComponent<Target>().SetTargetedState(false);
            }
        }
	}
}
