﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class Village : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField]
	private float force = 5f;
	[SerializeField]
	private float radius = 1f;
	[SerializeField]
	private float burnTime = 2f;
	private bool isBurning;
	public Vector2 grassSizeMinMax;

	[Header("References")]
	[SerializeField]
	private ParticleSystem smoke;
	[SerializeField]
	private GameObject rubbleDecal;
	[SerializeField]
	private GameObject cracksDecal;
	[SerializeField]
	private GameObject fireFX;
	private Grass[] grasses;
	private Rigidbody[] physComps;
	private Rock[] rocks;
	[SerializeField]
	private PeasantTrigger[] peasants;
	[SerializeField]
	private SheepLogic[] sheep;
	[SerializeField]
	private GameObject burningHouseSound;

	private void Awake()
	{
		physComps = GetComponentsInChildren<Rigidbody>();
		grasses = GetComponentsInChildren<Grass>();
		rocks = GetComponentsInChildren<Rock>();
		peasants = GetComponentsInChildren<PeasantTrigger>();
		sheep = GetComponentsInChildren<SheepLogic>();
	}
	
	public void GetStomped()
	{
		isBurning = true;
		StartCoroutine(ExecuteStomp());
	}

	private IEnumerator ExecuteStomp()
	{
		yield return new WaitForSeconds(HandTracking.Instance.stompDelay);
		rubbleDecal.SetActive(true);
		cracksDecal.SetActive(true);
		smoke.gameObject.SetActive(true);
		fireFX.gameObject.SetActive(false);

		foreach (Rock r in rocks)
			r.ExecuteStomp();

		foreach (Rigidbody rb in physComps)
		{
			rb.isKinematic = false;
			rb.AddExplosionForce(force, transform.position, radius);
		}

		PlaneFinding.Instance.StartRespawnEnemy(transform.position, this, null);
	}

	public void GetBurned()
	{
		if (isBurning)
			return;

		StartCoroutine(Burning());

		foreach(PeasantTrigger pt in peasants)
			pt.GetComponent<Animator>().SetTrigger("Burn");

		foreach (SheepLogic sl in sheep)
			sl.Burn();

		foreach (Grass g in grasses)
			g.gameObject.SetActive(false);
	}

	private IEnumerator Burning()
	{
		fireFX.SetActive(true);
		isBurning = true;
		smoke.gameObject.SetActive(true);
		burningHouseSound.SetActive(true);
		yield return new WaitForSeconds(burnTime);
		rubbleDecal.SetActive(true);
		rubbleDecal.transform.SetParent(null);
		PlaneFinding.Instance.StartRespawnEnemy(transform.position, null, rubbleDecal);
		gameObject.SetActive(false);
	}
}