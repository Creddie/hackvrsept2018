﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
	private Rigidbody thisRB;

	private void Awake()
	{
		thisRB = GetComponent<Rigidbody>();
	}

	public void ExecuteStomp()
	{
		thisRB.isKinematic = false;
	}
}