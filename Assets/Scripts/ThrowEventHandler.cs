﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowEventHandler : MonoBehaviour
{
	[HideInInspector]
    public GameObject projectileSocket;
	[HideInInspector]
	public GameObject projectileToUse;
    public int damagePerShot;
	[HideInInspector]
	public GameObject player;
	private AudioSource aSource;

	private void Awake()
	{
		aSource = GetComponent<AudioSource>();
	}

	void Throw()
    {
        aSource.Play();
        var newProjectile = Instantiate(projectileToUse, projectileSocket.transform.position, Quaternion.identity);
        Projectile projectileComponent = newProjectile.GetComponent<Projectile>();
        projectileComponent.SetDamage(damagePerShot);
        projectileComponent.targetPos = player.transform.position;
        projectileComponent.target = player;
    }
}
